## Introduction ##

Hello, We are really glad, that you are interested in our job offer.

To test your skills, we found an interesting task that can help us to understand how you are thinking about the problem but not take much of your precious time.

## Rules ##

* Use HTML, CSS, and JS (React framework is preferred)

## Task ##

Create a single page application that allows users to search for books using the [Google Books API](https://developers.google.com/books). The application should include the following features:

* A search bar that allows users to enter a query and search for books
* A list of search results that displays the title, author, and cover image of each book
* The ability to view more details about a book, including a description and button which allows open book on https://books.google.com/
* Add the ability for users to save their favorite books to a "My Books" page. This page should display the books that the user has favorite and allow them to remove them from their favorites (here choose a solution which you think is the best one, we can discuss other solutions later)
* Design simple interface using CSS or help yourself with some framework

## Bonus ##

If you think the task was too easy you can also implement:

* Implement solution with server-side rendering
* Implement pagination for the search results, allowing users to view 10 results per page and navigate to the next or previous page
* Application optimized for search engines

## Delivery ##

( Create git repository (github, bitbucket...) with your code
`OR`
pack your code to .zip )
`AND`
send link or file to dominika.neprasova@boataround.com and lukas.machacek@dev.boataround.com

Please also provide instructions on how to run it.

## Last words ##

Thank you very much for the time you spend on this test, we really appreciate it! Good luck and in case of any questions/issues, please forward the e-mail to lukas.machacek@dev.boataround.com